My personal website, located at [jnewport.dev](https://jnewport.dev).

Features no Javascript and minimal CSS; best viewed in any web browser.
